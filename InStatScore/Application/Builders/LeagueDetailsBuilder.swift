//
//  LeagueDetailBuilder.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

final class LeagueDetailsBuilder {
    func buildLeagueDetailsView(router: LeagueDetailsRouterProtocol, league: League) -> LeagueDetailsView {
        let leagueDetailsView = LeagueDetailsViewController()
        leagueDetailsView.presenter = buildLeagueDetailsPresenter(view: leagueDetailsView, router: router, league: league)
        return leagueDetailsView
    }

    private func buildLeagueDetailsPresenter(view: LeagueDetailsView, router: LeagueDetailsRouterProtocol, league: League) -> LeagueDetailsPresenterProtocol {
        return LeagueDetailsPresenter(view: view, router: router, league: league)
    }
}
