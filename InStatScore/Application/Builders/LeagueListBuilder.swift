//
//  LeagueListBuilder.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

final class LeagueListBuilder {
    
    func buildLeagueListView(router: LeagueListRouterProtocol) -> LeagueViewController {
        let leagueViewController = LeagueViewController()
        leagueViewController.presenter = buildLeagueListPresenter(view: leagueViewController, router: router)
        return leagueViewController
    }

    private func buildLeagueListPresenter(view: LeagueListView, router: LeagueListRouterProtocol) -> LeagueListPresenterProtocol {
        return LeagueListPresenter(view: view, router: router)
    }
}
