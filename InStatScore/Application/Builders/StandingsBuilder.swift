//
//  StandingsBuilder.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

final class StandingsBuilder {
    func buildStandingsView(router: StandingsRouterProtocol, leagueId: String, selectedIndex: Int, seasons: [Season]) -> StandingsView {
        let standingsView = StandingsViewController()
        standingsView.presenter = buildStandingsPresenter(view: standingsView, router: router, leagueId: leagueId, selectedIndex: selectedIndex, seasons: seasons)
        return standingsView
    }

    private func buildStandingsPresenter(view: StandingsView, router: StandingsRouterProtocol, leagueId: String, selectedIndex: Int, seasons: [Season]) -> StandingsPresenterProtocol {
        return StandingsPresenter(view: view, router: router, leagueId: leagueId, selectedIndex: selectedIndex, seasons: seasons)
    }
}
