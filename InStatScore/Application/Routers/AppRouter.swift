//
//  AppRouter.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

import UIKit

final class AppRouter {
    private let navigationController: UINavigationController
    private let appBuilder: AppBuilder
    private var leagueListRouter: LeagueListRouter?

    init(navigationController: UINavigationController, appBuilder: AppBuilder) {
        self.navigationController = navigationController
        self.appBuilder = appBuilder
    }

    func start() {
        let leagueListRouter = LeagueListRouter(navigationController: navigationController)
        leagueListRouter.start()
        self.leagueListRouter = leagueListRouter
    }
}
