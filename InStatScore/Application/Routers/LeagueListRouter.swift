//
//  LeagueListRouter.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import UIKit

final class LeagueListRouter: LeagueListRouterProtocol, LeagueDetailsRouterProtocol, StandingsRouterProtocol {
    func showStandings(model: League, selectedIndex: Int, seasons: [Season]) {
        guard let standingController = StandingsBuilder().buildStandingsView(router: self, leagueId: model.id ?? "", selectedIndex: selectedIndex, seasons: seasons) as? UIViewController else {
            fatalError("StandingsViewController not built")
        }
        standingController.title = "\(model.name ?? "") \(seasons[selectedIndex].year)"
        standingController.view.backgroundColor = .systemBackground
        navigationController?.pushViewController(standingController,
                                                 animated: true)
    }
    
    
    func showLeagueDetails(model: League) {
        guard let leagueDetailsViewController = LeagueDetailsBuilder().buildLeagueDetailsView(router: self, league: model) as? UIViewController else {
            fatalError("LeagueDetailsViewController not built")
        }
        leagueDetailsViewController.title = model.name
        leagueDetailsViewController.view.backgroundColor = .systemBackground
        navigationController?.pushViewController(leagueDetailsViewController,
                                                 animated: true)
    }
    
    private weak var navigationController: UINavigationController?
    private weak var leagueViewController: UIViewController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let leagueViewController = LeagueListBuilder().buildLeagueListView(router: self)
        leagueViewController.title = "Leagues"
        leagueViewController.view.backgroundColor = .systemBackground
        navigationController?.pushViewController(leagueViewController, animated: false)
        self.leagueViewController = leagueViewController
    }
}


protocol LeagueListRouterProtocol: AnyObject {
    func showLeagueDetails(model: League)
}
protocol LeagueDetailsRouterProtocol: AnyObject {
    func showStandings(model: League, selectedIndex: Int, seasons: [Season])
}
protocol StandingsRouterProtocol: AnyObject {
    
}
