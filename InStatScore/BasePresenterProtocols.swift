//
//  BasePresenterProtocols.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import UIKit

protocol PresenterProtocol {
    func viewLoaded()
    func viewWillAppear()
}

extension PresenterProtocol {
    func viewLoaded() {}
    func viewWillAppear() {}
}

protocol Alertable {}

extension Alertable where Self: UIViewController {
    func showAlert(title: String = "",
                   actionTitle: String,
                   message: String,
                   preferredStyle: UIAlertController.Style = .alert,
                   actionHandler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: actionTitle, style: UIAlertAction.Style.default, handler: actionHandler))
        self.present(alert, animated: true, completion: nil)
    }
}
