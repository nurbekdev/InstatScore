//
//  UITableView+Additions.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import UIKit

extension UITableView {
    func dequeueCell<T>(at indexPath: IndexPath)  -> T where  T: UITableViewCell {
        
            guard let cell = dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T else {
              fatalError("Unexpected ReusableCell Type for reuseID \(T.identifier)")
            }
            return cell
      }

    func dequeueView<T>() -> T where  T: UITableViewHeaderFooterView {
        
            guard let cell = dequeueReusableHeaderFooterView(withIdentifier: T.identifier) as? T else {
                fatalError("Unexpected ReusableView Type for reuseID \(T.identifier)")
            }
            return cell
    }
}
