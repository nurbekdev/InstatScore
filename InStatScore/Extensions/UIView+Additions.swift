//
//  UIView+Additions.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import UIKit

extension UIView {
    static var identifier: String {
        return String(describing: self)
    }
    func addSubviews(_ views: UIView...) {
        views.forEach {
            addSubview($0)
        }
    }
}
