//
//  LeagueModels.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

struct LeaguesRequest: Decodable {
    let data: [League]
}

struct League: Decodable {
    let id: String?
    let name: String?
    let slug: String?
    let abbr: String?
    let logos: LeagueLogo?
}
struct LeagueLogo: Decodable {
    let light: String?
    let dark: String?
}


extension NetworkManager {
    func getLeagues(completion: @escaping ([League]?, String?) -> Void) {
        provider.request(.leagues) { data, response, error in
            if error != nil {
                completion(nil, "Please check your network connection.")
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    print(responseData)
                    guard let result = self.returnObject(of: LeaguesRequest.self, from: responseData).0 else {
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                        return
                    }
                    completion(result.data, nil)
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
}
