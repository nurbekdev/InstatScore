//
//  LeaguePresenter.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation


protocol LeagueListPresenterProtocol: PresenterProtocol, TableViewDataSource {
    func didTapRetryOption()
    func didSelectRow(indexPath: IndexPath)
    func handleLeagueRequest()
}

enum LeagueListViewState: Equatable {
    case clear
    case loading
    case render
    
    case error(message: String)
    
    struct ViewModel: Equatable {
        let rowsViewModels: [LeagueCellViewModel]
    }
}

protocol LeagueListView {
    var presenter: LeagueListPresenterProtocol! { get set }
    func changeViewState(viewState: LeagueListViewState)
}

final class LeagueListPresenter: LeagueListPresenterProtocol {
    
    private var view: LeagueListView?
    private weak var router: LeagueListRouterProtocol!
    private var provider: Networkable = NetworkManager()
    private var leagues: [League]?

    private var viewState: LeagueListViewState = .clear {
        didSet {
            guard oldValue != viewState else {
                return
            }
            view?.changeViewState(viewState: viewState)
        }
    }

    init(view: LeagueListView?,
         router: LeagueListRouterProtocol) {
        self.view = view
        self.router = router
    }

    func viewLoaded() {
        handleLeagueRequest()
    }

    func didSelectRow(indexPath: IndexPath) {
        guard let leagues = leagues else {
            return
        }
        let selectedLeagueModel = leagues[indexPath.row]
        router.showLeagueDetails(model: selectedLeagueModel)
    }
    
    func didTapRetryOption() {
        handleLeagueRequest()
    }
    
    func handleLeagueRequest() {
        viewState = .loading
        provider.getLeagues { [self] leagues, message in
            guard let leagues = leagues else {
                self.viewState = .error(message: message ?? "")
                return
            }
            viewState = .render
            self.leagues = leagues
        }
    }
    private func getCellViewModel(league: League) -> LeagueCellViewModel {
        return LeagueCellViewModel(logoURL: URL(string: league.logos?.light ?? ""), abbreviation: league.abbr ?? "", leagueName: league.name ?? "")
    }
}

extension LeagueListPresenter: TableViewDataSource {
    func numberOfRowsInSection(section: Int) -> Int {
        leagues?.count ?? 0
    }

    func viewModelForCell(at indexPath: IndexPath) -> CellViewModel? {
        guard let league = leagues?[indexPath.row] else {
            return nil
        }
        let cellViewModel = getCellViewModel(league: league)
        return cellViewModel
    }
}
