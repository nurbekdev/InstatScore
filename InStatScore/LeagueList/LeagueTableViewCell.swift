//
//  LeagueTableViewCell.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import UIKit

struct LeagueCellViewModel: CellViewModel, Equatable {
    let logoURL: URL?
    let abbreviation: String
    let leagueName: String?
}

class LeagueTableViewCell: UITableViewCell {

    private let logoImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private let abbreviationLabel: UILabel = {
        let label = UILabel()
        label.textColor = .systemGray
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    private let leagueNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        logoImageView.image = nil
        abbreviationLabel.text = nil
        leagueNameLabel.text = nil
    }
    func setup(viewModel: LeagueCellViewModel) {
        if let url = viewModel.logoURL {
            logoImageView.downloadAndShowImage(url: url)
        }
        abbreviationLabel.text = viewModel.abbreviation
        leagueNameLabel.text = viewModel.leagueName
    }
    private func setupViews() {
        selectionStyle = .none
        [logoImageView, abbreviationLabel, leagueNameLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        addSubviews(logoImageView, abbreviationLabel, leagueNameLabel)
        
        logoImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        logoImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        abbreviationLabel.leftAnchor.constraint(equalTo: logoImageView.rightAnchor, constant: 10).isActive = true
        abbreviationLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 20).isActive = true
        abbreviationLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        
        leagueNameLabel.leftAnchor.constraint(equalTo: logoImageView.rightAnchor, constant: 10).isActive = true
        leagueNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 20).isActive = true
        leagueNameLabel.topAnchor.constraint(equalTo: abbreviationLabel.bottomAnchor, constant: 5).isActive = true
        leagueNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
}
