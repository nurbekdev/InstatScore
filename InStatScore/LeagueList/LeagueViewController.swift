//
//  MainViewController.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import UIKit

class LeagueViewController: UIViewController, LeagueListView, Alertable {
    
    var presenter: LeagueListPresenterProtocol!
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(LeagueTableViewCell.self, forCellReuseIdentifier: LeagueTableViewCell.identifier)
        return tableView
    }()
    
    private var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewLoaded()
        setupViews()
    }
    private func setupViews() {
        view.addSubviews(tableView, activityIndicator)
        setupConstraints()
    }
    private func setupConstraints() {
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func changeViewState(viewState: LeagueListViewState) {
        switch viewState {
        case .render:
            DispatchQueue.main.async { [self] in
                activityIndicator.stopAnimating()
                tableView.isUserInteractionEnabled = true
                tableView.reloadData()
            }
        case .loading:
            DispatchQueue.main.async { [self] in
                activityIndicator.isHidden = false
                tableView.isUserInteractionEnabled = false
                activityIndicator.startAnimating()
            }
            break
        case .clear:
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        case .error(let message):
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.showAlert(title: "Error", actionTitle: "Retry", message: message) { [weak self] _ in
                    self?.presenter.didTapRetryOption()
                }
            }
            
        }
    }
    
    
}
extension LeagueViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: LeagueTableViewCell = tableView.dequeueCell(at: indexPath)
        if let cellViewModel = presenter.viewModelForCell(at: indexPath) as? LeagueCellViewModel {
            cell.setup(viewModel: cellViewModel)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectRow(indexPath: indexPath)
    }
    
}
