//
//  FootballEndPoint.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import Foundation

enum NetworkEnvironment {
    case qa
}

public enum FootballLigueAPI: TargetType {
    
    case leagues
    case leaguesDetail(id: String)
    case seasons(id: String)
    case standings(id: String, season: String)
    
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .qa:
            return "https://api-football-standings.azharimm.site"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .leagues:
            return "leagues"
        case .leaguesDetail(let id):
            return "leagues/\(id)"
        case .seasons(let id):
            return "leagues/\(id)/seasons"
        case .standings(let id, _):
            return "leagues/\(id)/standings"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        default:
            return .get
        }
        
    }
    
    var task: HTTPTask {
        switch self {
        case .standings(_, let season):
            return.requestParameters(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: [
                "season": season,
                "sort": "asc"
            ])
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        let assigned: [String: String] = [
            "Accept-Language": "",
            "Accept": "*/*",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        switch self {
            
        default:
            return assigned
        }
    }
    
    
}
