//
//  NetworkManager.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import Foundation

protocol Networkable {
    var provider: Router<FootballLigueAPI> { get }
    func getLeagues(completion: @escaping([League]?, String?) -> Void)
    func getSeasons(leagueId: String, completion: @escaping([Season]?, String?) -> Void)
    func getStandings(leagueId: String, season: String, completion: @escaping([Standings]?, String?) -> Void)
}

enum Result<String>{
    case success
    case failure(String)
}

enum NetworkResponse:String {
    case success
    case authenticationError = "You need to be authenticated first."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

struct NetworkManager: Networkable {
    var provider: Router<FootballLigueAPI> = Router<FootballLigueAPI>()
    
    
    static let environment : NetworkEnvironment = .qa
    
    func returnObject<T: Decodable>(of type: T.Type, from data: Data) -> (T?, String?) {
        do {
            if let object = try? JSONDecoder().decode(T.self, from: data) {
                return (object, nil)
            } else {
                return (nil, String(data: data, encoding:.utf8) ?? "")
            }
        }
    }
    
    func returnObject<T: Decodable>(of type: T.Type, from data: Data, with key: String) -> (T?, String?) {
        if let data = try? JSONDecoder().decode([String: T].self, from: data), let object = data[key] {
            return (object, nil)
        } else {
            return (nil, String(data: data, encoding:.utf8) ?? "")
        }
    }
    func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}
