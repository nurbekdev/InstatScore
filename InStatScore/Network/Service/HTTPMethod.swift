//
//  HTTPMethod.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import Foundation

public enum HTTPMethod : String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
}
