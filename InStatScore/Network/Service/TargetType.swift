//
//  TargetType.swift
//  InStatScore
//
//  Created by Nurbek on 03.08.2022.
//

import Foundation

protocol TargetType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
