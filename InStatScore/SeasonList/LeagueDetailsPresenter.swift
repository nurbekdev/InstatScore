//
//  LeagueDetailsPresenter.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

protocol LeagueDetailsView {
    var presenter: LeagueDetailsPresenterProtocol! { get set }
    func changeViewState(viewState: LeagueDetailsViewState)
}

protocol LeagueDetailsPresenterProtocol: PresenterProtocol, TableViewDataSource {
    func didTapRetryOption()
    func didSelectRow(indexPath: IndexPath)
    func handleSeasonsRequest()
}

enum LeagueDetailsViewState: Equatable {
    case clear
    case loading
    case render
    
    case error(message: String)
    
    struct ViewModel: Equatable {
        let rowsViewModels: [SeasonsCellViewModel]
    }
}

final class LeagueDetailsPresenter: LeagueDetailsPresenterProtocol {
    
    private var view: LeagueDetailsView?

    private var router: LeagueDetailsRouterProtocol!
    private var league: League!
    private var seasons: [Season]!
    private var provider: Networkable = NetworkManager()
    private var viewState: LeagueDetailsViewState = .clear {
        didSet {
            guard oldValue != viewState else {
                return
            }
            view?.changeViewState(viewState: viewState)
        }
    }

    init(view: LeagueDetailsView?,
         router: LeagueDetailsRouterProtocol,
         league: League) {
        self.view = view
        self.router = router
        self.league = league
    }
    func viewLoaded() {
        handleSeasonsRequest()
    }
    func didTapRetryOption() {
        handleSeasonsRequest()
    }
    
    func didSelectRow(indexPath: IndexPath) {
        guard let seasons = seasons else {
            return
        }
        router.showStandings(model: league, selectedIndex: indexPath.row, seasons: seasons)
    }
    
    func handleSeasonsRequest() {
        viewState = .loading
        provider.getSeasons(leagueId: league.id ?? "") { [self] seasons, message in
            guard let seasons = seasons else {
                self.viewState = .error(message: message ?? "")
                return
            }
            viewState = .render
            self.seasons = seasons
        }
    }
    private func getCellViewModel(season: Season) -> SeasonsCellViewModel {
        return SeasonsCellViewModel(startDate: season.startDate, endDate: season.endDate, seasonName: season.displayName)
    }
}
extension LeagueDetailsPresenter: TableViewDataSource {
    func numberOfRowsInSection(section: Int) -> Int {
        seasons?.count ?? 0
    }

    func viewModelForCell(at indexPath: IndexPath) -> CellViewModel? {
        guard let season = seasons?[indexPath.row] else {
            return nil
        }
        let cellViewModel = getCellViewModel(season: season)
        return cellViewModel
    }
}
