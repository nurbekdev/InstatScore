//
//  SeasonModels.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

struct SeasonsRequest: Decodable {
    let data: LeagueDescription
}
struct LeagueDescription: Decodable {
    let seasons: [Season]
}
struct Season: Decodable {
    let year: Int
    let displayName: String?
    let startDate: String?
    let endDate: String?
    let types: [SeasonTypes]
}
struct SeasonTypes: Decodable {
    let hasStandings: Bool
}

extension NetworkManager {
    func getSeasons(leagueId: String, completion: @escaping ([Season]?, String?) -> Void) {
        provider.request(.seasons(id: leagueId)) { data, response, error in
            if error != nil {
                completion(nil, "Please check your network connection.")
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    print(responseData)
                    guard let result = self.returnObject(of: SeasonsRequest.self, from: responseData).0 else {
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                        return
                    }
                    completion(result.data.seasons, nil)
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
}
