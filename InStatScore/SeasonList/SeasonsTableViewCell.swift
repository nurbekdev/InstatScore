//
//  SeasonsTableViewCell.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import UIKit

struct SeasonsCellViewModel: CellViewModel, Equatable {
    let startDate: String?
    let endDate: String?
    let seasonName: String?
}

class SeasonsTableViewCell: UITableViewCell {
    
    private let seasonNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        return label
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        seasonNameLabel.text = nil
        dateLabel.text = nil
    }
    
    func setup(viewModel: SeasonsCellViewModel) {
        selectionStyle = .none
        seasonNameLabel.text = viewModel.seasonName
        dateLabel.text = "\(viewModel.startDate ?? "") - \(viewModel.endDate ?? "")"
    }
    
    private func setupViews() {
        [seasonNameLabel, dateLabel].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        addSubviews(seasonNameLabel, dateLabel)
        
        seasonNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        seasonNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        seasonNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        
        dateLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        dateLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        dateLabel.topAnchor.constraint(equalTo: seasonNameLabel.bottomAnchor, constant: 7).isActive = true
        dateLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
}
