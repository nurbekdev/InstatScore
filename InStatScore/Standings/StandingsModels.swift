//
//  StandingsModels.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

struct StandingsRequest: Decodable {
    let data: SeasonDescription
}
struct SeasonDescription: Decodable {
    let standings: [Standings]
}
struct Standings: Decodable {
    let team: Team? 
    let note: StandingsNote?
    let stats: [StandingsStatistic]
}
struct Team: Decodable {
    let id: String?
    let name: String?
    let logos: [TeamLogos]?
}
struct TeamLogos: Decodable {
    let href: String?
}
struct StandingsNote: Decodable {
    let color: String?
}
struct StandingsStatistic: Decodable {
    let abbreviation: String?
    let displayValue: String?
}

extension NetworkManager {
    func getStandings(leagueId: String, season: String, completion: @escaping ([Standings]?, String?) -> Void) {
        provider.request(.standings(id: leagueId, season: season)) { data, response, error in
            if error != nil {
                completion(nil, "Please check your network connection.")
            }
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    print(responseData)
                    guard let result = self.returnObject(of: StandingsRequest.self, from: responseData).0 else {
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                        return
                    }
                    completion(result.data.standings, nil)
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
}
