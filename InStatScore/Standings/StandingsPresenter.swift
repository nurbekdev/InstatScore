//
//  StandingsPresenter.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import Foundation

protocol StandingsView {
    var presenter: StandingsPresenterProtocol! { get set }
    func changeViewState(viewState: StandingsViewState)
}

protocol StandingsPresenterProtocol: PresenterProtocol, TableViewDataSource {
    func didTapRetryOption()
    func handleStandingsRequest()
}

enum StandingsViewState: Equatable {
    case clear
    case loading
    case render
    
    case error(message: String)
    
    struct ViewModel: Equatable {
        let rowsViewModels: [StandingsCellViewModel]
//        let availableSeasons: [Season]
    }
}

final class StandingsPresenter: StandingsPresenterProtocol {
    
    
    
    private var view: StandingsView?

    private var router: StandingsRouterProtocol!
    private var leagueId: String!
    private var seasons: [Season]!
    private var standings: [Standings]?
    private var selectedIndex: Int
    private var provider: Networkable = NetworkManager()
    private var viewState: StandingsViewState = .clear {
        didSet {
            guard oldValue != viewState else {
                return
            }
            view?.changeViewState(viewState: viewState)
        }
    }

    init(view: StandingsView, router: StandingsRouterProtocol, leagueId: String, selectedIndex: Int, seasons: [Season]) {
        self.view = view
        self.router = router
        self.leagueId = leagueId
        self.seasons = seasons
        self.selectedIndex = selectedIndex
    }
    func viewLoaded() {
        handleStandingsRequest()
    }
    func didTapRetryOption() {
        handleStandingsRequest()
    }
    func handleStandingsRequest() {
        viewState = .loading
        provider.getStandings(leagueId: leagueId, season: "\(seasons[selectedIndex].year)") { [self] standings, message in
            guard let standings = standings else {
                self.viewState = .error(message: message ?? "")
                return
            }
            viewState = .render
            self.standings = standings
        }
    }
    private func getCellViewModel(standings: Standings) -> StandingsCellViewModel {
        return StandingsCellViewModel(rank: nil, color: standings.note?.color, teamLogo: URL(string: standings.team?.logos?[0].href ?? "") , teamName: standings.team?.name, GP: standings.stats[3].displayValue, W: standings.stats[0].displayValue, D: standings.stats[2].displayValue, L: standings.stats[1].displayValue, G: "\(standings.stats[4].displayValue ?? ""):\(standings.stats[5].displayValue ?? "")", P: standings.stats[6].displayValue)
    }
}
extension StandingsPresenter: TableViewDataSource {
    func numberOfRowsInSection(section: Int) -> Int {
        return standings?.count ?? 0
    }

    func viewModelForCell(at indexPath: IndexPath) -> CellViewModel? {
        guard let standing = standings?[indexPath.row] else {
            return nil
        }
        let cellViewModel = getCellViewModel(standings: standing)
        return cellViewModel
    }
}
