//
//  StandingsTableViewCell.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import UIKit


struct StandingsCellViewModel: CellViewModel, Equatable {
    let rank: String?
    let color: String?
    let teamLogo: URL?
    let teamName: String?
    let GP: String?
    let W: String?
    let D: String?
    let L: String?
    let G: String?
    let P: String?
}

class StandingsTableViewCell: UITableViewCell {
    
    private let rankLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.layer.masksToBounds = true
        label.layer.cornerRadius = 5
        return label
    }()
    private let teamNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
//        label.numberOfLines = 1
//        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    private let GPLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    private let GLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    private let PLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    private let logoImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        stackView.axis = .horizontal
        stackView.addArrangedSubview(GPLabel)
        stackView.addArrangedSubview(GLabel)
        stackView.addArrangedSubview(PLabel)
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func prepareForReuse() {
        rankLabel.text = nil
        teamNameLabel.text = nil
        GPLabel.text = nil
        GLabel.text = nil
        PLabel.text = nil
        logoImageView.image = nil
    }
    
    func setup(viewModel: StandingsCellViewModel, rowIndex: Int) {
        selectionStyle = .none
        if let url = viewModel.teamLogo {
            logoImageView.downloadAndShowImage(url: url)
        }
        if let hexColor = viewModel.color, let color = UIColor(hex: hexColor)  {
            rankLabel.backgroundColor = color
        }
        rankLabel.text = "\(rowIndex+1)"
        teamNameLabel.text = viewModel.teamName
        GPLabel.text = viewModel.GP
        GLabel.text = viewModel.G
        PLabel.text = viewModel.P
        print(viewModel.color, "gg")
    }
    
    private func setupViews() {
        [rankLabel, teamNameLabel, stackView, logoImageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        addSubviews(rankLabel, teamNameLabel, stackView, logoImageView)
        stackView.addSubviews(GPLabel, GLabel, PLabel)
        
        rankLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 5).isActive = true
        rankLabel.widthAnchor.constraint(equalToConstant: 25).isActive = true
        rankLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        logoImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        logoImageView.leftAnchor.constraint(equalTo: rankLabel.rightAnchor, constant: 2).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        logoImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
//        logoImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        
        teamNameLabel.leftAnchor.constraint(equalTo: logoImageView.rightAnchor, constant: 5).isActive = true
        teamNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 11).isActive = true
        teamNameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -11).isActive = true
        
        stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        stackView.leftAnchor.constraint(equalTo: teamNameLabel.rightAnchor, constant: 15).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        stackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.33).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
