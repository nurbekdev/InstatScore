//
//  StandingsViewController.swift
//  InStatScore
//
//  Created by Nurbek on 04.08.2022.
//

import UIKit

class StandingsViewController: UIViewController, StandingsView, Alertable {
    
    var presenter: StandingsPresenterProtocol!
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(StandingsTableViewCell.self, forCellReuseIdentifier: StandingsTableViewCell.identifier)
        return tableView
    }()
    
    private var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewLoaded()
        setupViews()
        // Do any additional setup after loading the view.
    }
    private func setupViews() {
        view.addSubviews(tableView, activityIndicator)
        setupConstraints()
    }
    private func setupConstraints() {
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    
    func changeViewState(viewState: StandingsViewState) {
        switch viewState {
        case .render:
            DispatchQueue.main.async { [self] in
                activityIndicator.stopAnimating()
                tableView.isUserInteractionEnabled = true
                tableView.reloadData()
            }
        case .loading:
            DispatchQueue.main.async { [self] in
                activityIndicator.isHidden = false
                tableView.isUserInteractionEnabled = false
                activityIndicator.startAnimating()
            }
            break
        case .clear:
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        case .error(let message):
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.showAlert(title: "Error", actionTitle: "Retry", message: message) { [weak self] _ in
                    self?.presenter.didTapRetryOption()
                }
            }
            
        }
    }
    
}
extension StandingsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfRowsInSection(section: section)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 44))
        headerView.backgroundColor = .systemBackground
        let rankLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            label.text = "#"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.layer.cornerRadius = 5
            return label
        }()
        let teamNameLabel: UILabel = {
            let label = UILabel()
            label.text = "TEAM"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.numberOfLines = 1
            label.adjustsFontSizeToFitWidth = true
            return label
        }()
        let GPLabel: UILabel = {
            let label = UILabel()
            label.text = "GP"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textAlignment = .center
            return label
        }()
        let GLabel: UILabel = {
            let label = UILabel()
            label.text = "G"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textAlignment = .center
            return label
        }()
        let PLabel: UILabel = {
            let label = UILabel()
            label.text = "P"
            label.font = UIFont.boldSystemFont(ofSize: 15)
            label.textAlignment = .center
            return label
        }()
        let stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.distribution = .fillEqually
            stackView.spacing = 0
            stackView.axis = .horizontal
            stackView.addArrangedSubview(GPLabel)
            stackView.addArrangedSubview(GLabel)
            stackView.addArrangedSubview(PLabel)
            return stackView
        }()
        
        [rankLabel, teamNameLabel, stackView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        headerView.addSubviews(rankLabel, teamNameLabel, stackView)
        stackView.addSubviews(GPLabel, GLabel, PLabel)
        
        rankLabel.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 5).isActive = true
        rankLabel.widthAnchor.constraint(equalToConstant: 25).isActive = true
        rankLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        
        teamNameLabel.leftAnchor.constraint(equalTo: rankLabel.rightAnchor, constant: 40).isActive = true
        teamNameLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        
        stackView.rightAnchor.constraint(equalTo: headerView.rightAnchor, constant: -5).isActive = true
        stackView.leftAnchor.constraint(equalTo: teamNameLabel.rightAnchor, constant: 15).isActive = true
//        stackView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        stackView.widthAnchor.constraint(equalTo: headerView.widthAnchor, multiplier: 0.33).isActive = true
        stackView.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StandingsTableViewCell = tableView.dequeueCell(at: indexPath)
        
        if let cellViewModel = presenter.viewModelForCell(at: indexPath) as? StandingsCellViewModel {
            cell.setup(viewModel: cellViewModel, rowIndex: indexPath.row)
        }
        return cell
    }
    
    
}
